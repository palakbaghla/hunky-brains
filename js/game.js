var currentQuestion=0;
var score=0;
var totalquestion= question.length;

var container=document.getElementById('quizcontainer');
var questionEl=document.getElementById('question');
var opt1=document.getElementById('opt1');
var opt2=document.getElementById('opt2');
var opt3=document.getElementById('opt3');
var opt4=document.getElementById('opt4');
var resultCounter=document.getElementById('result');
var submit=document.getElementById('submit');

function loadQuestion(questionIndex){
    var q = question[questionIndex];
    questionEl.textContent=(questionIndex + 1) +'. '+q.question;
    opt1.textContent=q.option1;
    opt2.textContent=q.option2;
    opt3.textContent=q.option3;
    opt4.textContent=q.option4;
};

function load(){
    var selectedOption = document.querySelector('input[type=radio]:checked');
    if(!selectedOption){
        alert('please select your answer!');
        return;
    }

    var answer = selectedOption.value;
    if(question[currentQuestion].answer == answer){
        score=score+1;
    } 

    selectedOption.checked = false;
    currentQuestion++;
    
   }

    loadQuestion(currentQuestion);

}

    loadQuestion(currentQuestion);

